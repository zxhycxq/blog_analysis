# 热榜文章的主题分布：你可以使用词云或柱状图等可视化工具，对热榜文章的标题进行分词和统计，看看哪些主题最受关注。
# 例如，你可以发现，人工智能、SpringBoot、SpringCloud等技术主题比较热门。
import os
from collections import Counter
from datetime import datetime
import pandas as pd
import jieba


class CategoryStatistical:
    def __init__(self):
        self.counter = {}

        self.my_BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        # csv_path：读取的csv文件(爬虫产物)
        self.csv_path = os.path.join(self.my_BASE_DIR, 'static', 'csv_collect', 'deep_articles.csv')
        # categoryTXT_path：从csv中筛选出的待分类文本
        self.categoryTXT_path = os.path.join(self.my_BASE_DIR, 'static', 'txt_collect', 'category.txt')
        # first_selectTXT_path：用于制作jiaba自定义字典，可以往里面补充互联网词汇
        self.first_selectTXT_path = os.path.join(self.my_BASE_DIR, 'static', 'txt_collect', 'first_select.txt')
        # jiebaDirectTXT_path：从first_select.txt生成的可供程序运行的jieba自定义字典
        self.jiebaDirectTXT_path = os.path.join(self.my_BASE_DIR, 'static', 'txt_collect', 'jieba_directory.txt')

    def run(self):

        # 1、读取csv数据
        df = pd.read_csv(self.csv_path)

        # 2、生成自定义jieba字典-----------------------没必要每次都做，影响速度
        # self.generate_jiebaDirect()

        # 3、制作待分类文本
        # 3、1 为了保证每次都是生产出新的category待分类文本
        #      先判断文件是否存在,存在先删掉
        # if os.path.exists(self.categoryTXT_path):
        #     os.remove(self.categoryTXT_path)

        # 3、2 制作新的category待分类文本
        # for i in range(len(df)):
        #     self.add_txt(df["category"][i])

        # -------------------------------------------没必要每次都做，影响速度

        # 4、使用jieba库进行分词，并萃取统计出词频
        self.extract_jieba()
        # print(self.counter)
        return self.counter

    # 自定义jieba字典
    # 结巴库首次错乱数据，自己手动筛出first_select.txt，利用这个生成jieba_directory.txt  作为jieba字典
    def generate_jiebaDirect(self):
        list_direct = []
        # 打开手动处理一半的结巴初筛结果
        with open(self.first_selectTXT_path, 'r', encoding="utf-8") as f:
            lines = f.readlines()
            for line in lines:
                # 如果读到空行，就跳过
                if line.isspace():
                    continue
                else:
                    # 去除文本中的换行等等，可以追加其他操作
                    line = line.replace("\n", "")
                    line = line.replace("\t", "")
                    # 处理完成后的行，追加到列表中
                    list_direct.append(line)

        # 利用set集合不可重复的特性去重
        set_direct = set(list_direct)

        #      先判断文件是否存在,存在先删掉
        if os.path.exists(self.jiebaDirectTXT_path):
            os.remove(self.jiebaDirectTXT_path)

        # 然后遍历集合写入jieba_directory
        with open(self.jiebaDirectTXT_path, 'a', encoding="utf-8") as f:
            for item in iter(set_direct):
                if item == "js":
                    f.write(f"{item} 10\n")
                else:
                    f.write(f"{item} 1000\n")

    # 写入category.txt作为待分类文本，需要被遍历调用
    def add_txt(self, category_str):
        # 清洗数据
        # 给定的时间字符串可能为NOTFOUND或者空字符
        if "" != category_str and "NOTFOUND" != category_str:
            # 先遍历全部category写入txt文件
            with open(self.categoryTXT_path, 'a', encoding="utf-8") as f:
                f.write(category_str.replace("\n", ""))

    # 利用jieba字典，统计词频
    def extract_jieba(self):
        # 使用自己定制的jieba字典库
        jieba.load_userdict(self.jiebaDirectTXT_path)
        # 这是你的字符串
        with open(self.categoryTXT_path, 'r', encoding="utf-8") as f:
            category_txt_string = f.read().rstrip("\n")

            # 使用jieba进行分词
            words = jieba.lcut(category_txt_string)

            # 使用Counter统计每个词语出现的次数
            temp_counter = Counter(words)
            # print(temp_counter)
            # 打印结果
            for word, count in temp_counter.items():
                # print(f"词语'{word}'出现的次数：{count}")

                # 过滤错误分词数据
                if word == "." or word == "-" or word == "/" or word == "·" or word == "6" or word == " ":
                    continue
                else:
                    self.counter[word] = count

            # 使用sorted()函数和lambda函数，按照字典的值排序
            # sorted()函数返回一个由键值对组成的列表
            sorted_list = sorted(self.counter.items(), key=lambda x: x[1],reverse=True)

            # 使用dict()函数，将列表转换为一个新的字典
            self.counter = dict(sorted_list)


# 以脚本方式启动
if __name__ == "__main__":
    # 捕捉异常错误
    try:
        cs = CategoryStatistical()
        cs.run()
    except Exception as e:
        print("错误:", e)
