"""
URL configuration for blog_analysis project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.urls import path
# django项目目录下的urls.py，让首页显示打包的index.html文件
from django.views.generic import TemplateView
from django.views.generic import RedirectView

from blog_analysis import views

urlpatterns = [
    path('', RedirectView.as_view(url='/static/index.html', permanent=False), name='index'),
    path("login", views.login, name="login"),  # 1、登录验证
    path("register", views.register, name="register"),  # 2、注册用户
    path("timeRank", views.timeRank, name="timeRank"),  # 3、文章的时间分布（饼图和柱状图）
    path("category", views.category, name="category"),  # 4、文章的类别分布（饼图和柱状图）
    path("title", views.title, name="title"),  # 5、文章的主题分布
    path("topic", views.topic, name="topic"),  # 6、时下的热门话题分布分析（）
    path("interact", views.interact, name="interact"),  # 7、文章互动分析-----散点图
    path("influence", views.influence, name="influence"),  # 8、文章影响力分析----散点图
    path("someone", views.someone, name="someone"),  # 9、简单爬取展示 目标用户信息
    path("laborRank", views.laborRank, name="laborRank"),  # 10、创作者文章数排行
    path("UserPicture", views.UserPicture, name="UserPicture"),  # 11、 上传用户头像 http://127.0.0.1:8888/static/picture/xXxX.jpg

    # 下面这玩意可以忽略了，直接在上面写路径
    path('admin/', admin.site.urls),
]
