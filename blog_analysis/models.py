'''涵盖项目使用到的bean 对应数据库各张表结构'''
from django.db import models
import datetime


# Create your models here.

# 实体类
class Meta:
    """元信息 在一个http请求完成后与data一起打包成JSON数据发给API调用者
            @attribute: msg = str
            @attribute: status = int
            """
    # 属性
    msg = str
    status = int

    def __init__(self, msg: str, status: int):
        """订单表:Order实体类的构造函数"""
        self.msg = msg
        self.status = status

    def toString(self):
        return self.__str__()

    def __str__(self):
        return f"Meta(msg={self.msg}, status={self.status})"

    def __repr__(self):
        return f"Meta(msg={self.msg}, status={self.status})"

    def __json__(self):
        return {"msg": self.msg, "status": self.status}


class User:
    """用户实体类
            @attribute:  user_id =str
            @attribute:  username = int
            @attribute:  password = str
            @attribute:  level = int
            @attribute:  grade = str
            @attribute:  type = str
            @attribute:  wallet = int
            @attribute:  user_picture = str
            @attribute:  state_message = str"""
    # 属性
    user_id = str
    username = int
    password = str
    level = int
    grade = str
    type = str
    wallet = int
    user_picture = str
    state_message = str

    # 构造函数
    def __init__(self, user_id: int, username: str, password: str, level: int, grade: str, type: str, wallet: int,
                 user_picture: str, state_message: str):
        """User实体类的构造函数"""
        self.user_id = user_id
        self.username = username
        self.password = password
        self.level = level
        self.grade = grade
        self.type = type
        self.wallet = wallet
        self.user_picture = user_picture
        self.state_message = state_message

    def __str__(self):
        return f"User(user_id={self.user_id}, username={self.username},password={self.password},level={self.level},grade={self.grade},type={self.type},wallet={self.wallet},user_picture={self.user_picture},state_message={self.state_message})"

    def __repr__(self):
        return f"User(user_id={self.user_id}, username={self.username},password={self.password},level={self.level},grade={self.grade},type={self.type},wallet={self.wallet},user_picture={self.user_picture},state_message={self.state_message})"

    def __json__(self):
        return {"user_id": self.user_id,
                "username": self.username,
                "password": self.password,
                "level": self.level,
                "grade": self.grade,
                "type": self.type,
                "wallet": self.wallet,
                "user_picture": self.user_picture,
                "state_message": self.state_message}


class Article:
    """文章详情实体类"""

    def __init__(self, url, title, time, watchs, readDuration, column, likes, comments, stars, author, author_url,
                 author_lever, author_articles, author_watchs, author_fans, category, topic):
        self.url = url
        self.title = title
        self.time = time
        self.watchs = watchs
        self.readDuration = readDuration
        self.column = column
        self.likes = likes
        self.comments = comments
        self.stars = stars
        self.author = author
        self.author_url = author_url
        self.author_lever = author_lever
        self.author_articles = author_articles
        self.author_watchs = author_watchs
        self.author_fans = author_fans
        self.category = category
        self.topic = topic

    def toString(self):
        return self.__str__()

    def __json__(self):
        return {"url": self.url,
                "title": self.title,
                "time": self.time,
                "watchs": self.watchs,
                "readDuration": self.readDuration,
                "column": self.column,
                "likes": self.likes,
                "comments": self.comments,
                "stars":self.stars,
                "author": self.author,
                "author_url": self.author_url,
                "author_lever": self.author_lever,
                "author_articles": self.author_articles,
                "author_watchs": self.author_watchs,
                "author_fans": self.author_fans,
                "category": self.category,
                "topic": self.topic}
