# 导入pandas模块
import pandas as pd
import os
import openpyxl

class MyFormat(object):

    def run(self):
        paths = [
            # './csv_collect/big_articles.csv',
            './csv_collect/deep_articles.csv',
            # './csv_collect/userImfo.csv',
            # './csv_collect/user_all_articles.csv',
        ]

        for item in paths:
            self.csv_to_excel(item)

    def csv_to_excel(self, csv_file_path="./csv_collect/big_articles.csv"):
        # 指定csv文件的路径和名称
        # 读取csv文件，并返回一个DataFrame对象
        df = pd.read_csv(csv_file_path, sep=",", header=0, encoding="utf-8")

        # 指定excel文件的路径和名称
        file_name, file_ext = os.path.splitext(csv_file_path)  # 返回一个元组，('E:\\Python\\Path\\test', '.py')
        excel_file = file_name + ".xlsx"

        # 将DataFrame对象生成为一个excel文件
        df.to_excel(excel_file, index=False, sheet_name="data")


# 以脚本方式启动
if __name__ == "__main__":
    # 捕捉异常错误
    try:
        spider = MyFormat()
        spider.run()
    except Exception as e:
        print("错误:", e)
