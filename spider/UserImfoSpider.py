import os
import time as time_sleep
from datetime import datetime
from selenium import webdriver
import csv

from selenium.common import NoSuchElementException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By



# 定义一个爬虫类
class UserImfoSpider(object):
    # 初始化
    # 定义初始页面url
    def __init__(self):
        self.driver = None

    # 主函数
    def run(self, url="https://juejin.cn/user/501033035374045/posts"):
        # 大部分的优化手段都在options中进行设置
        chrome_options = Options()
        chrome_options.add_argument("--window-size=1920,1080")
        # chrome_options.add_argument("--disable-extensions")
        # chrome_options.add_argument("--headless")
        # chrome_options.add_argument("--disable-gpu")
        # chrome_options.add_argument("--disable-software-rasterizer")
        # chrome_options.add_argument('--no-sandbox')
        # chrome_options.add_argument('--ignore-certificate-errors')
        # chrome_options.add_argument('--allow-running-insecure-content')
        # chrome_options.add_argument("blink-settings=imagesEnabled=false")
        # 设置chromedriver，并打开webdriver  参数options=chrome_options
        self.driver = webdriver.Chrome(options=chrome_options)
        # self.driver = webdriver.Chrome()
        self.driver.get(url)
        self.driver.implicitly_wait(2)  # seconds

        # 此处写下 爬取哪块栏目 的函数
        self.sp_userImfo(url)

        # 关闭webdriver
        self.driver.close()

    # 解析页面，数据清洗
    # 个人信息
    def sp_userImfo(self, pass_url):
        driver = self.driver

        url = pass_url
        avatar = "NOTFOUND"
        author = "NOTFOUND"
        author_lever = "NOTFOUND"
        friend_lever = "NOTFOUND"
        achieves = "NOTFOUND"
        focuse = "NOTFOUND"
        be_focuse = "NOTFOUND"
        badge = "NOTFOUND"
        join_time = "NOTFOUND"

        try:
            avatar = driver.find_element(By.CSS_SELECTOR,
                                        '#juejin > div.view-container > main > div > div.major-area > div.user-info-block.block.shadow > div.avatar.jj-avatar > img').get_attribute("src")
        except NoSuchElementException:
            avatar = "NOTFOUND"

        time_sleep.sleep(2)  # 等一会，等加载数据

        try:
            author = driver.find_element(By.CSS_SELECTOR,
                                       '#juejin > div.view-container > main > div > div.major-area > div.user-info-block.block.shadow > div.info-box.info-box > div.top > div.left > h1 > span').text
        except NoSuchElementException:
            author = "NOTFOUND"

        try:
            author_lever = driver.find_element(By.CSS_SELECTOR,
                                         '#juejin > div.view-container > main > div > div.major-area > div.user-info-block.block.shadow > div.info-box.info-box > div.user-info-icon > span.rank.rank > img').get_attribute("title")
        except NoSuchElementException:
            author_lever = "NOTFOUND"

        try:
            friend_lever = driver.find_element(By.CSS_SELECTOR,
                                               '#juejin > div.view-container > main > div > div.major-area > div.user-info-block.block.shadow > div.info-box.info-box > div.user-info-icon > span.jueyou-level > span > img').get_attribute("title")
        except NoSuchElementException:
            friend_lever = "NOTFOUND"

        try:
            achieves = driver.find_element(By.CSS_SELECTOR,
                                         '#juejin > div.view-container > main > div > div.minor-area > div > div.stat-block.block.shadow > div.block-body').text
        except NoSuchElementException:
            achieves = "NOTFOUND"

        try:
            focuse = driver.find_element(By.CSS_SELECTOR,
                                        '#juejin > div.view-container > main > div > div.minor-area > div > div.follow-block.block.shadow > a:nth-child(1) > div.item-count').text
        except NoSuchElementException:
            focuse = "NOTFOUND"

        try:
            be_focuse = driver.find_element(By.CSS_SELECTOR,
                                           '#juejin > div.view-container > main > div > div.minor-area > div > div.follow-block.block.shadow > a:nth-child(2) > div.item-count').text
        except NoSuchElementException:
            be_focuse = "NOTFOUND"

        try:
            badge = driver.find_element(By.CSS_SELECTOR,
                                        '#juejin > div.view-container > main > div > div.major-area > a > div > div > div').text
        except NoSuchElementException:
            badge = "NOTFOUND"

        try:
            join_time = driver.find_element(By.CSS_SELECTOR,
                                         '#juejin > div.view-container > main > div > div.minor-area > div > div.more-block.block > div > div.item-count > time').text
        except NoSuchElementException:
            join_time = "NOTFOUND"

       

        # 保存数据
        # 判断文件是否存在
        if os.path.exists("csv_collect/userImfo.csv"):
            with open("csv_collect/userImfo.csv", "a+", newline="", encoding="utf-8") as f:
                # 生成csv操作对象
                writer = csv.writer(f)
                csv_data = [
                    url,
                    avatar,
                    author,
                    author_lever,
                    friend_lever,
                    achieves,
                    focuse,
                    be_focuse,
                    badge,
                    join_time
                ]
                writer.writerow(csv_data)
        else:
            with open("csv_collect/userImfo.csv", "a+", newline="", encoding="utf-8") as f:
                # 生成csv操作对象
                writer = csv.writer(f)
                # 制作表头
                csv_title = [
                    "url",
                    "avatar",
                    "author",
                    "author_lever",
                    "friend_lever",
                    "achieves",
                    "focuse",
                    "be_focuse",
                    "badge",
                    "join_time"
                ]
                csv_data = [
                    url,
                    avatar,
                    author,
                    author_lever,
                    friend_lever,
                    achieves,
                    focuse,
                    be_focuse,
                    badge,
                    join_time
                ]
                writer.writerow(csv_title)
                writer.writerow(csv_data)

        # 控制台提示信息
        print("---------UserImfoSpider----爬取用户信息: " + author + " 成功----------")


# 以脚本方式启动
if __name__ == "__main__":
    # 捕捉异常错误
    try:
        spider = UserImfoSpider()
        spider.run()
    except Exception as e:
        print("错误:", e)