# 稀土掘金·博客数据可视化

关键词：Vue、python、Django、Numpy、Pandas·····

做什么：对稀土掘金博客进行数据收集、数据预处理、数据存储、数据处理与分析、数据可视化。

项目所分析的最新数据集最后截止日期：2023年12月14日

>  主要做什么：

- **数据源展示**：展示所爬取的文章数据(后端直接返回11772条数据)，支持分页（在前端分页）
- **文章热度分析**：创建一个综合指标，考虑观看次数、点赞数、评论数和星级，以衡量每篇文章的影响力。使用基于加权平均的热度计算：热度 = w1 × 点赞数 + w2 × 评论数 + w3 × 收藏数
- **创作时间分析**：分析总体用户的创作时间偏好：周一到周五的文章比周末的文章多，按小时划分文章发布时间段
- **文章标签分布**：统计了文章的标签分布情况 
- **创作话题分布**：统计了文章所关联的掘金官方话题 分布情况 
- **创作标题分布**：统计了掘金创作者最喜欢使用的文章标题词汇，以及技术点。使用jieba库分词，和词云、柱状图来展示。由于词云是实时的需要耗费时间，还加了进度条等待。
- **创作者排行**：按创作者文章数量进行排行，选出靠前的劳模

> 收获：

- **从周时间分布上看**：工作日，用户有更多的时间和精力进行写作。周末写作意愿低。
- **从24小时分布上看**：上午8点至11点以及下午2点至5点的写作量较高。
- **从热度上看**：影响力高的文章，标题都很有趣，有深度有思考。
- **从文章标签**：掘金博客的内容覆盖面广泛，吸引了不同领域的技术人员参与交流和分享。但是也可以看出前端开发是掘金博客的主要内容，也是读者最感兴趣的话题。
- **从创作标题上看**：“如何”，达到了776次，说明掘金博客包含了诸多编程问题的解决方法。
- **从劳模作者排行看**：具有发展潜力的作者有量子位、终有救赎、Yiko、anyup···等，值得关注。
- ·····
- ~~编不下去了😜😜😜（手动狗头护体）~~

# 如何启动

## 1、安装所需依赖：
```
pip install -r requirements.txt 
```
由于爬虫使用到了selenium，所以有需要最新数据集的 请自己去配置chrome驱动

当然，你也可以不爬，直接用我上万条数据的数据集，就在

`项目根目录/static/csv_collect/deep_articles.csv`


## 2、附加数据库

把 mysql_databases.sql附加到你的MySQL中

## 3、启动项目
```
 python .\manage.py runserver 8888
```

# 运行效果



## 登录注册

### 登录
![](https://gitee.com/kang-zhenbin/blog_analysis/raw/master/img/login.png)
### 注册
![](https://gitee.com/kang-zhenbin/blog_analysis/raw/master/img/register.png)

## 主页

### 数据源展示可视化
![](https://gitee.com/kang-zhenbin/blog_analysis/raw/master/img/AnSomeone.png)


### 文章热度分析可视化
![](https://gitee.com/kang-zhenbin/blog_analysis/raw/master/img/AnInfluence.png)


### 创作时间分布可视化
![](https://gitee.com/kang-zhenbin/blog_analysis/raw/master/img/AnTimeRank.png)


### 文章标签标签分布可视化
![](https://gitee.com/kang-zhenbin/blog_analysis/raw/master/img/AnCategory.png)

![](https://gitee.com/kang-zhenbin/blog_analysis/raw/master/img/AnCategory2.png)


### 文章话题分布可视化
![](https://gitee.com/kang-zhenbin/blog_analysis/raw/master/img/AnTopic.png)

![](https://gitee.com/kang-zhenbin/blog_analysis/raw/master/img/AnTopic2.png)


### 文章标题分布可视化
![](https://gitee.com/kang-zhenbin/blog_analysis/raw/master/img/AnTitle.png)

![](https://gitee.com/kang-zhenbin/blog_analysis/raw/master/img/AnTitle2.png)

![](https://gitee.com/kang-zhenbin/blog_analysis/raw/master/img/AnTitle3.png)

![](https://gitee.com/kang-zhenbin/blog_analysis/raw/master/img/AnTitle4.png)


### 创作者排行可视化
![](https://gitee.com/kang-zhenbin/blog_analysis/raw/master/img/AnLaborRank.png)

![](https://gitee.com/kang-zhenbin/blog_analysis/raw/master/img/AnLaborRank2.png)



# 关于项目文件的简单介绍

## 数据采集
本项目爬虫由多个爬虫类同时组成，并可以使用多线程技术来加速数据爬取。

当然，多线程爬虫有问题爬不动，不过




![](https://gitee.com/kang-zhenbin/blog_analysis/raw/master/img/spider.png)

- BigArticlesSpider.py 在稀土掘金首页无限滚动刷新追加数据，最后爬取一万条基础数据
- SingleArticleSpider.py 针对单篇文章进行数据爬取，供其他类遍历可实现一万条数据的深度信息爬取。
- DeepDigBigArticles.py 对一万多条基础数据进行遍历，深度爬取信息
- ThreedAccelerateSpider.py 上面爬虫类的升级版，使用多线程技术，加速爬取
- UserAllArticles.py 针对用户专栏，爬取用户的所有文章
- UserlmfoSpider.py 针对用户主页，爬取用户的信息
- MyFormat.py 对爬取生成的csv文件进行转换，产出excel文件



##  数据分析处理

多线程加速数据分析实现了，就在`dataProcess\ThreedAccelerateLaborStatistical.py`，芜湖！CPU小风扇哐哐转起来！

![](https://gitee.com/kang-zhenbin/blog_analysis/raw/master/img/dataProcess.png)

-  CategoryStatistical.py 文章标签分布：统计文章标签的词频，展示各类主题的关注度。
-  InfluenceStatistical.py 文章热度分析：基于加权平均的热度分析计算，量化每篇文章的传播热度。
-  LaborStatistical.py 创作者排行
-  SomeoneStatistical.py 将爬虫爬取存在服务器的数据以json格式返回给前端
-  ThreedAccelerateLaborStatiscal.py 创作者排行：使用多线程技术快速统计每个作者的创作数量，评估其影响力。
-  TimeStatistical.py 创作时间分布：统计用户在一周内每天的创作数和24小时内每小时的创作数，揭示创作时间规律。
-  TitleStatistical.py 创作标题分布：统计文章标题的词频，了解标题的语言风格和习惯。
-  TopicStatistical.py 创作话题分布：统计文章话题的词频，展示热门话题及其变化趋势。
-  Drawlmg.py 使用matplotlib绘制可视化图像



## 数据可视化
这部分属于前端
-  AnSomeone.vue 数据源展示可视化
-  AnInfluence.vue 文章热度分析可视化
-  AnTimeRank.vue 创作时间分布可视化
-  AnCategory.vue 文章标签标签分布可视化
-  AnTopic.vue  文章话题分布可视化
-  AnTitle.vue 文章标题分布可视化
-  AnLaborRank.vue 创作者排行可视化
-  CoreLogin.vue  登录组件
-  CoreRegister.vue 注册组件

## blog_analysis

这部分属于基本的django应用

![](https://gitee.com/kang-zhenbin/blog_analysis/raw/master/img/django.png)