-- MySQL dump 10.13  Distrib 5.7.38, for Win64 (x86_64)
--
-- Host: localhost    Database: blog_analysis
-- ------------------------------------------------------
-- Server version	5.7.38

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `grade` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `wallet` int(11) DEFAULT NULL,
  `user_picture` text,
  `state_message` text,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','123456',1,'vip','root',2967776,'http://127.0.0.1:8888/static/picture/kzb68up.jpg','杩欎釜浜哄緢鎳掞紝浠€涔堥兘娌℃湁鐣欎笅'),(4,'鍐伴晣鐢熼矞','123456',1,'vip','root',-14187,'http://127.0.0.1:8888/static/picture/kzb68up.jpg','杩欎釜浜哄緢鎳掞紝浠€涔堥兘娌℃湁鐣欎笅'),(6,'kzb','123456',1,'vip','root',1300,'http://127.0.0.1:8888/static/picture/kzb68up.jpg','杩欎釜浜哄緢鎳掞紝浠€涔堥兘娌℃湁鐣欎笅'),(28,'娴嬭瘯璐﹀彿','123456',1,'vip','root',1000,'http://127.0.0.1:8888/users_picture/kzb68up.jpg','杩欎釜浜哄緢鎳掞紝浠€涔堥兘娌℃湁鐣欎笅'),(29,'娴嬭瘯001','123456',1,'vip','normal',1000,'http://127.0.0.1:8888/static/picture/0e647132c895d143b18a46ee36f0820258af0747.jpg','杩欎釜浜哄緢鎳掞紝浠€涔堥兘娌℃湁鐣欎笅'),(30,'娴嬭瘯001','123456',1,'vip','normal',1000,'http://127.0.0.1:8888/static/picture/0e647132c895d143b18a46ee36f0820258af0747.jpg','杩欎釜浜哄緢鎳掞紝浠€涔堥兘娌℃湁鐣欎笅'),(31,'','',1,'vip','root',1000,'','杩欎釜浜哄緢鎳掞紝浠€涔堥兘娌℃湁鐣欎笅'),(32,'娴嬭瘯璐﹀彿32','123456',1,'vip','root',1000,'http://127.0.0.1:8888/users_picture/kzb68up.jpg','杩欎釜浜哄緢鎳掞紝浠€涔堥兘娌℃湁鐣欎笅');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-01-08 12:00:38
